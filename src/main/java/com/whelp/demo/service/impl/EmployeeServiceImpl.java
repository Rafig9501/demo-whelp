package com.whelp.demo.service.impl;

import com.whelp.demo.dto.EmployeeDto;
import com.whelp.demo.entity.Employee;
import com.whelp.demo.repository.EmployeeRepository;
import com.whelp.demo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public ResponseEntity<List<Employee>> getAllEmployees() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(employeeRepository.findAll());
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @Override
    public ResponseEntity<Employee> getEmployeeById(Long id) {
        try {
            Optional<Employee> optionalEmployee = employeeRepository.findById(id);
            return optionalEmployee.map(employee -> ResponseEntity.status(HttpStatus.OK).body(employee))
                    .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @Override
    public ResponseEntity<Employee> createEmployee(EmployeeDto employeeDto) {
        try {
            Employee employee = Employee.builder()
                    .name(employeeDto.getName())
                    .surname(employeeDto.getSurname())
                    .salary(employeeDto.getSalary()).build();
            return ResponseEntity.status(HttpStatus.CREATED).body(employeeRepository.save(employee));
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @Override
    public ResponseEntity<Employee> updateEmployee(Long employeeId, EmployeeDto employeeDto) {
        try {
            Optional<Employee> optionalEmployee = employeeRepository.findById(employeeId);
            return optionalEmployee.map(employee -> {
                employee.setName(employeeDto.getName());
                employee.setSurname(employeeDto.getSurname());
                employee.setSalary(employeeDto.getSalary());
                return ResponseEntity.status(HttpStatus.OK).body(employeeRepository.save(employee));
            }).orElseGet(() -> createEmployee(employeeDto));
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @Override
    public HttpStatus deleteEmployee(Long id) {
        try {
            Optional<Employee> optionalEmployee = employeeRepository.findById(id);
            return optionalEmployee.map(employee -> {
                employeeRepository.delete(employee);
                return HttpStatus.NO_CONTENT;
            }).orElse(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error(e.getMessage());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
