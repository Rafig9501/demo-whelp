package com.whelp.demo.service;

import com.whelp.demo.dto.EmployeeDto;
import com.whelp.demo.entity.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public interface EmployeeService {

    ResponseEntity<List<Employee>> getAllEmployees();

    ResponseEntity<Employee> getEmployeeById(Long id);

    ResponseEntity<Employee> createEmployee(EmployeeDto employeeDto);

    ResponseEntity<Employee> updateEmployee(Long employeeId, EmployeeDto employeeDto);

    HttpStatus deleteEmployee(Long id);
}
