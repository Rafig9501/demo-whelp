package com.whelp.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class EmployeeDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    @NotBlank(message = "Name cannot be empty")
    @NotNull(message = "Name cannot be null")
    @Length(min = 3, max = 50)
    private String name;

    @JsonProperty("surname")
    @NotBlank(message = "Surname cannot be empty")
    @NotNull(message = "Surname cannot be null")
    @Length(min = 3, max = 50)
    private String surname;

    @JsonProperty("salary")
//    @NotBlank(message = "Salary cannot be empty")
    @NotNull(message = "Salary cannot be null")
    @DecimalMin("0")
    private BigDecimal salary;
}
