package com.whelp.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhelpApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhelpApplication.class, args);
    }

}
