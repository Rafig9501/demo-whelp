package com.whelp.demo.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.whelp.demo.dto.EmployeeDto;
import com.whelp.demo.entity.Employee;
import com.whelp.demo.repository.EmployeeRepository;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {EmployeeServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class EmployeeServiceImplTest {

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeServiceImpl employeeServiceImpl;

    @Test
    public void testGetAllEmployees() {
        ResponseEntity<List<Employee>> actualAllEmployees = this.employeeServiceImpl.getAllEmployees();
        assertEquals("<200 OK OK,[],[]>", actualAllEmployees.toString());
        assertTrue(actualAllEmployees.hasBody());
        assertEquals(HttpStatus.OK, actualAllEmployees.getStatusCode());
        assertTrue(actualAllEmployees.getHeaders().isEmpty());
    }

    @Test
    public void testGetEmployeeById() {
        ResponseEntity<Employee> actualEmployeeById = this.employeeServiceImpl.getEmployeeById(123L);
        assertNull(actualEmployeeById.getBody());
        assertEquals("<404 NOT_FOUND Not Found,[]>", actualEmployeeById.toString());
        assertEquals(HttpStatus.NOT_FOUND, actualEmployeeById.getStatusCode());
        assertTrue(actualEmployeeById.getHeaders().isEmpty());
    }

    @Test
    public void testCreateEmployee() {
        ResponseEntity<Employee> actualCreateEmployeeResult = this.employeeServiceImpl.createEmployee(EmployeeDto.builder()
                .name("John").surname("Adams").salary(new BigDecimal("10.54542")).build());
        assertNull(actualCreateEmployeeResult.getBody());
        assertEquals("<201 CREATED Created,[]>", actualCreateEmployeeResult.toString());
        assertEquals(HttpStatus.CREATED, actualCreateEmployeeResult.getStatusCode());
        assertTrue(actualCreateEmployeeResult.getHeaders().isEmpty());
    }

    @Test
    public void testCreateEmployee2() {
        ResponseEntity<Employee> actualCreateEmployeeResult = this.employeeServiceImpl.createEmployee(null);
        assertNull(actualCreateEmployeeResult.getBody());
        assertEquals("<500 INTERNAL_SERVER_ERROR Internal Server Error,[]>", actualCreateEmployeeResult.toString());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, actualCreateEmployeeResult.getStatusCode());
        assertTrue(actualCreateEmployeeResult.getHeaders().isEmpty());
    }

    @Test
    public void testUpdateEmployee() {
        ResponseEntity<Employee> actualUpdateEmployeeResult = this.employeeServiceImpl.updateEmployee(123L,
                EmployeeDto.builder().name("John").surname("Adams").salary(new BigDecimal("10.54542")).build());
        assertNull(actualUpdateEmployeeResult.getBody());
        assertEquals("<201 CREATED Created,[]>", actualUpdateEmployeeResult.toString());
        assertEquals(HttpStatus.CREATED, actualUpdateEmployeeResult.getStatusCode());
        assertTrue(actualUpdateEmployeeResult.getHeaders().isEmpty());
    }

    @Test
    public void testDeleteEmployee() {
        assertEquals(HttpStatus.NOT_FOUND, this.employeeServiceImpl.deleteEmployee(123L));
    }
}

