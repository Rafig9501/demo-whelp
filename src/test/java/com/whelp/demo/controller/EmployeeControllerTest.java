package com.whelp.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whelp.demo.dto.EmployeeDto;
import com.whelp.demo.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {EmployeeController.class})
@ExtendWith(SpringExtension.class)
public class EmployeeControllerTest {

    @Autowired
    private EmployeeController employeeController;

    @MockBean
    private EmployeeService employeeService;

    @Test
    public void testGetAllEmployees() throws Exception {
        when(this.employeeService.getAllEmployees()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employee/employees");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.employeeController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        when(this.employeeService.getEmployeeById(any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employee/employee/{id}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.employeeController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    @Test
    public void testCreateEmployee() throws Exception {
        when(this.employeeService.createEmployee(any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setSalary(BigDecimal.valueOf(42L));
        employeeDto.setId(123L);
        employeeDto.setName("Name");
        employeeDto.setSurname("Doe");
        String content = (new ObjectMapper()).writeValueAsString(employeeDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/employee/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.employeeController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    @Test
    public void testUpdateEmployee() throws Exception {
        when(this.employeeService.updateEmployee(any(), any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setSalary(BigDecimal.valueOf(42L));
        employeeDto.setId(123L);
        employeeDto.setName("Name");
        employeeDto.setSurname("Doe");
        String content = (new ObjectMapper()).writeValueAsString(employeeDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/employee/employees/{id}", 123L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.employeeController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/employee/employees/{id}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.employeeController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(415));
    }
}
