Employee REST API


Task is to build simple REST API with the following requirements:

1. Endpoints - 50 points
2. Bonus paging endpoint - 10 points
3. Validation - 10 points
4. Unit tests - 20 points
5. Clean code - 10 points

# Endpoints:

1. GET /employees - returns all list of employees with HTTP Status 200
2. GET /employees/{id} -
    1. returns employee specified by id, HTTP Status 200
    2. return HTTP Status 404 if employee not found
3. POST /employees -
    1. create new employee from request body, return HTTP Status 201
    2. do not use request params
    3. validate input, i
    4. return new created employee as response including id
4. PUT /employees/{id}
    1. if employee found, update from request body
    2. else create new employee from request body
    3. return HTTP Status 200
5. DELETE /employees/{id}
    1. if employee found, delete and return HTTP status 204
    2. return HTTP Status 404 if employee not found
6. BONUS GET /employees/list - implement paging for the list of employees
    1. Request parameters:
- pageNum - integer, default value 1
- pageSize - integer, default value 10
- sortBy - String, default value is empty, property to order result
- sortDir - String, default value asc, sort order
- filter - String, default value is empty, used for searching
1. returns matching page of of employees with HTTP Status 200

# Data model

## 1.Employee

- Long id - min 0, can not be negative
- String name - min 3, max 50 characters, can not be empty or null
- String surname - min 3, max 50 characters, can not be empty or null
- BigDecimal salary - min 0.0, can not be empty, null or negative

## 2.PageResponse<T>

- List<T> data
- totalRecords - number of total employees
- totalPages - number of total pages calculated based on totalRecords and pageSize
- filteredRecords - number of matching filtered employees if filter is not empty
- filteredPages - number of matching filtered employees if filter is not empty

## Unit tests

Create necessary unit tests for endpoints with JUnit and Mockito.
